﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum Item {
	None, 
	Squirtle, 
	Shell,
	Shelf
}

public class ShelfMenuController : MonoBehaviour {

	// Outlets
	public Image slot1;
	public Image slot2;
	public Image slot3;

	// Configuration
	public Sprite imageSquirtle;
	public Sprite imageShell;
	public Sprite imageShelf;

	// State Tracking
	Item itemToPlace = Item.None;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	// 1) Select an Item
	public void ButtonPressedItemChoiceSquirtle() {
		itemToPlace = Item.Squirtle;
	}
	
	public void ButtonPressedItemChoiceShell() {
		itemToPlace = Item.Shell;
	}
	
	public void ButtonPressedItemChoiceShelf() {
		itemToPlace = Item.Shelf;
	}
	
	// 2) Place Item
	public void ButtonPressedSpot1() {
		if(itemToPlace != Item.None) {
			switch(itemToPlace) {
				case Item.Squirtle:
					slot1.sprite = imageSquirtle;
					break;
				case Item.Shell:
					slot1.sprite = imageShell;
					break;
				case Item.Shelf:
					slot1.sprite = imageShelf;
					break;
			}
		}
	}
	
	public void ButtonPressedSpot2() {
		if(itemToPlace != Item.None) {
			switch(itemToPlace) {
				case Item.Squirtle:
					slot2.sprite = imageSquirtle;
					break;
				case Item.Shell:
					slot2.sprite = imageShell;
					break;
				case Item.Shelf:
					slot2.sprite = imageShelf;
					break;
			}
		}
	}
	
	public void ButtonPressedSpot3() {
		if(itemToPlace != Item.None) {
			switch(itemToPlace) {
				case Item.Squirtle:
					slot3.sprite = imageSquirtle;
					break;
				case Item.Shell:
					slot3.sprite = imageShell;
					break;
				case Item.Shelf:
					slot3.sprite = imageShelf;
					break;
			}
		}
	}
}
