﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float movementAmount;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.UpArrow)) {
			Vector3 currentPosition = transform.position;
			currentPosition.y += movementAmount;
			transform.position = currentPosition;
		}
		if(Input.GetKey(KeyCode.DownArrow)) {
			Vector3 currentPosition = transform.position;
			currentPosition.y -= movementAmount;
			transform.position = currentPosition;
		}
	}
}
