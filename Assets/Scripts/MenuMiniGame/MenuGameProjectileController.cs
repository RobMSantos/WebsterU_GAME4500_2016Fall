﻿using UnityEngine;
using System.Collections;

public class MenuGameProjectileController : MonoBehaviour {

	// Configuration
	public int direction; // -1 = Left, 1 = Right
	public float speed;
	public float damage;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// Move in the targetted direction
		if(direction == -1) { // Left
			transform.Translate(new Vector3(-1f * speed, 0 ,0));
		} else if(direction == 1) { // Right
			transform.Translate(new Vector3(speed, 0 ,0));
		}
	}
	
	public void OnTriggerEnter2D(Collider2D collider) {
		// Disappear on touching enemy
		if(collider.gameObject.CompareTag("Enemy")) {
			Destroy(gameObject);
		}
	}
}
