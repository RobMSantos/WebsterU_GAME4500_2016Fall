﻿using UnityEngine;
using System.Collections;

public class MenuGameEnemyController : MonoBehaviour {

	// Outlets

	// Configuration
	public float speed;

	// State Tracking
	public float health;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// No behaviors if no player is available
		if(MenuGameController.instance.player == null) {
			return;
		}
	
		// Find player and move toward it
		float distanceFromPlayer = transform.position.x - MenuGameController.instance.player.transform.position.x;
		if(distanceFromPlayer < 0) {
			// Move Left
			transform.Translate(new Vector3(speed, 0 ,0));
		} else if(distanceFromPlayer > 0) {
			// Move Right
			transform.Translate(new Vector3(-1f * speed, 0 ,0));
		}
	}
	
	public void OnTriggerEnter2D(Collider2D collider) {
		// Get hurt by projectiles
		if(collider.gameObject.CompareTag("Projectile")) {
			MenuGameProjectileController projectileComponent = collider.GetComponent<MenuGameProjectileController> ();
			TakeDamage(projectileComponent.damage);
		}
	}
	
	public void OnTriggerStay2D(Collider2D collider) {
		// Hurt the player on touch
		if(collider.gameObject.CompareTag("Player")) {
			MenuGameController.instance.player.TakeDamage(1f);
		}
	}
	
	void TakeDamage(float amount) {
		health -= amount;
		if(health <= 0) {
			Die();
		}
	}
	
	void Die() {
		MenuGameController.instance.coins += 1;
		Destroy(gameObject);
	}
}
