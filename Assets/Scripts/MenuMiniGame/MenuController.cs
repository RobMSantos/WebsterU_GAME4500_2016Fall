﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

	public GameObject panelMenu;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update() {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			if(panelMenu.activeInHierarchy) {
				panelMenu.SetActive(false);
				Time.timeScale = 1;
			} else {
				panelMenu.SetActive(true);
				Time.timeScale = 0;
			}
		}
	}
	
	public void ButtonPressedOpenMenu() {
		panelMenu.SetActive(true);
		Time.timeScale = 0;
	}
	
	public void ButtonPressedCloseMenu() {
		panelMenu.SetActive(false);
		Time.timeScale = 1;
	}
}
