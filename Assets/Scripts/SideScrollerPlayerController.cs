﻿using UnityEngine;
using System.Collections;

public class SideScrollerPlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// Handle left-direction movement
		if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
			transform.Translate(new Vector3(-0.25f, 0, 0));
		}

		// Handle right-direction movement
		if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
			transform.Translate(new Vector3(0.25f, 0, 0));
		}
	}
}
