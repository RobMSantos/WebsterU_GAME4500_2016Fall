﻿using UnityEngine;
using System.Collections;

public class SquirtleController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// Grab our animator component
		Animator animatorController = GetComponent<Animator>();
		
		// Handle Left OR Right Controls
		if(Input.GetKeyDown(KeyCode.LeftArrow)) {
			animatorController.SetTrigger("WalkLeft");
		} else if(Input.GetKeyDown(KeyCode.RightArrow)) {
			animatorController.SetTrigger("WalkRight");
		}
	}
}
